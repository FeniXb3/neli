﻿using UnityEngine;
using System.Collections;

public class DestroyByBoundary : MonoBehaviour 
{
	void OnTriggerExit(Collider other)
	{
		if(other.transform.parent != null && other.transform.parent.gameObject.CompareTag("Projectile"))
			Destroy(other.transform.parent.gameObject);
		else if(other.gameObject.CompareTag("Player"))
		{
			other.gameObject.SendMessage("Kill");
		}
		else
		{
			Destroy(other.gameObject);
		}
	}
}
