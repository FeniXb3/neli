﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour 
{

	public GameObject gameController;
	public int cameraMoveDelay;

	private Vector3 offset;
	private bool followingProjectile;
	private float cameraMoveTime;

	void Start ()
	{
		offset = transform.position;
	}

	void LateUpdate () 
	{
		GameObject projectile = GameObject.FindGameObjectWithTag ("Projectile");
		if (projectile != null)
		{
			transform.position = projectile.transform.position + offset;
			followingProjectile = true;
		}
		else
		{
			if(followingProjectile)
			{
				cameraMoveTime += Time.deltaTime;

				if(cameraMoveTime >= cameraMoveDelay)
				{
					GameObject player = gameController.GetComponent<GameController>().ActivePlayer;
					if(player != null)
					{
						transform.position = player.transform.position + offset;
						gameController.GetComponent<GameController>().ChangeActivePlayer(false);
					}
				
					cameraMoveTime = 0;
					followingProjectile = false;
				}
			}
			else
			{
				GameObject player = gameController.GetComponent<GameController>().ActivePlayer;
				if(player.GetComponent<PlayerController>().State == PlayerStates.Dead)
					gameController.GetComponent<GameController>().ChangeActivePlayer(false);

				if(player != null)
					transform.position = player.transform.position + offset;
			}
		}
	}
}
