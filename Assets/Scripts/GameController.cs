﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GameController : MonoBehaviour {

	public GameObject character;
	public int teamsNumber = 2;
	public int teamSize = 3;
	public int xRange;
	
	//private GameObject team1;
	//private GameObject team2;
	private List<List<GameObject>> teams;
	private int activeTeamIndex;
	public GameObject ActivePlayer;

	public int ActiveTeamIndex
	{
		get
		{
			return activeTeamIndex;
		}
		set
		{
			activeTeamIndex = value % teams.Count; 
		}
	}

	void Awake ()
	{
		teams = new List<List<GameObject>>();

		for (int i = 0; i < teamsNumber; i++)
		{
			teams.Add(new List<GameObject>());
		}
		
		//List<Color> usedColors = new List<Color>();
		//List<int> takenPositions = new List<int>();
		
		foreach(var team in teams)
		{
			Color teamColor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));

			for(int i = 0; i < teamSize; i++)
			{
				Vector3 position = new Vector3(Random.Range(-xRange, xRange), 0f, 0f);
				GameObject newPlayer = Instantiate(character, position, Quaternion.identity) as GameObject;
				newPlayer.GetComponent<Renderer>().material.color = teamColor;
				team.Add(newPlayer);
				//newPlayer.transform.parent = team1.transform;
			}
		}

		ActivePlayer = teams[0][0];
		ActivePlayer.GetComponent<PlayerController>().State = PlayerStates.Active;
		activeTeamIndex = 0;
	}

	void Update()
	{
		if(Input.GetButtonUp("ChangeMember"))
			ChangeActivePlayer(true);
	}

	public void ChangeActivePlayer(bool theSameTeam)
	{
		if(theSameTeam)
		{
			if(teams[ActiveTeamIndex].Any(p => p.GetComponent<PlayerController>().State == PlayerStates.Idle))
			{
				//DeactivatePlayer(false);
				ActivatePlayer(DeactivatePlayer(false));
			}
		}
		else
		{
			DeactivatePlayer();
			ActiveTeamIndex++;

			if(ActivatePlayer() == null)
			{
				// all chars of next team are dead
				ChangeActivePlayer(false);
			}
		}
	}

	GameObject DeactivatePlayer(bool markAsDone = true)
	{
		if(ActivePlayer != null)
		{
			if(markAsDone)
				ActivePlayer.GetComponent<PlayerController>().State = PlayerStates.Done;
			else
				ActivePlayer.GetComponent<PlayerController>().State = PlayerStates.Idle;
		}

		return ActivePlayer;
	}

	GameObject ActivatePlayer(GameObject playerToIgnore = null)
	{
		ActivePlayer = teams[ActiveTeamIndex].FirstOrDefault(p => p.GetComponent<PlayerController>().State == PlayerStates.Idle && p != playerToIgnore);
		
		if(ActivePlayer == null)
		{
			var donePlayers = teams[ActiveTeamIndex].Where(p => p.GetComponent<PlayerController>().State == PlayerStates.Done).ToList();
			if(donePlayers != null)
			{
				for (int i = 0; i < donePlayers.Count(); i++) 
				{
					donePlayers[i].GetComponent<PlayerController>().State = PlayerStates.Idle;
				}
				
				ActivePlayer = teams[ActiveTeamIndex].FirstOrDefault(p => p.GetComponent<PlayerController>().State == PlayerStates.Idle);
			}
		}

		if(ActivePlayer != null)
			ActivePlayer.GetComponent<PlayerController>().State = PlayerStates.Active;

		return ActivePlayer;
	}
}
