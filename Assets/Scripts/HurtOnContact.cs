﻿using UnityEngine;
using System.Collections;

public class HurtOnContact : MonoBehaviour 
{
	public int damage = 10;

	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Boundary")
			return;

		if(other.tag != "Terrain")
		{
			other.gameObject.SendMessage("TakeDamage", damage);
		}
		else
		{
			var terrain = other.GetComponent<Terrain>();
			var terrainData = terrain.terrainData;

			var xRes = terrainData.heightmapWidth;
			var yRes = terrainData.heightmapHeight;
			var heights = terrainData.GetHeights(0, 0, xRes, yRes);

			var point = other.ClosestPointOnBounds(transform.position);

			int radius = 10;
			int py = (int)(((point.x - other.gameObject.transform.position.x) / terrainData.size.x) * xRes);

			for (int y = py-radius;  y < py + radius; y++) {
				for (int x = 0; x < xRes; x++) {
					if(x >= 0 && x < xRes && y >= 0 && y < yRes)
					{
						float heightRadius = radius / (float)yRes;//(radius / terrainData.size.x);
						float tmp = heightRadius;



						float newH = heights[x,y] -tmp;// (tmp * ((py - Mathf.Abs(py - y)) / (float)radius));
						if(newH < 0)
							newH = 0;

						if(x == 256)
						{
							Debug.Log (Mathf.Pow(y - py, 2) + Mathf.Pow(newH*512 - heights[x,py]*512, 2));
						}

						/*while(Mathf.Pow(y - py, 2) + Mathf.Pow(newH*512 - heights[x,py]*512, 2) >= Mathf.Pow(radius, 2))
						{
							tmp -= 0.001f;
							newH = heights[x,y] - tmp;
							if(newH < 0)
								newH = 0;
						}*/
						heights[x,y] = newH;

						//heights[x,y] = heights[x,y] - ((radius / (float)terrainData.size.x));//radius/10000.0f;//* 0.5f;
						/*

						Debug.Log (heights[x,y]);
						float tmp = radius/10000.0f;
						int i = 0;

						float newH = heights[x,y] - tmp;
						if(newH < 0)
							newH = 0;*/
						/*while(Mathf.Pow(y - py, 2) + Mathf.Pow(newH - heights[x,py], 2) <= Mathf.Pow(radius, 2))
						{
							//Debug.Log (newH);
							tmp -= 0.1f;
							newH = heights[x,y] - tmp;
							if(newH < 0)
								newH = 0;

							if(++i >= 10)
								break;
						}*/
					//	heights[x,y] = newH;//heights[x,y] - tmp
					}
				}
			}

			terrainData.SetHeights(0, 0, heights);
		}

		Destroy(gameObject);
	}

	void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.tag == "Boundary")
			return;
		
		if(other.gameObject.tag != "Terrain")
		{
			Destroy(other.gameObject);
		}
		else
		{
			var terrainData = other.gameObject.GetComponent<Terrain>().terrainData;
			
			//			var bounds = collider.bounds;
			//bounds.IntersectRay()
			var xRes = terrainData.heightmapWidth;
			var yRes = terrainData.heightmapHeight;
			var heights = terrainData.GetHeights(0, 0, xRes, yRes);

//			other.contacts


			for (int y = 0; y < yRes; y++) {
				for (int x = 0; x < xRes; x++) {
					heights[x,y] = heights[x,y] * 0.5f;
				}
			}
			
			terrainData.SetHeights(0, 0, heights);
		}
		
		Destroy(gameObject);
	}
}
