﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour {

	public float speed;
	// Use this for initialization
	void Start ()
	{
		GetComponent<Rigidbody>().AddForce (transform.up * speed);
	}
	
	// Update is called once per frame
	void Update () {
		transform.up =
			Vector3.Slerp(transform.up, GetComponent<Rigidbody>().velocity.normalized, Time.deltaTime);
	}
}
