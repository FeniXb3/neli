﻿using UnityEngine;
using System.Collections;

public class WeaponController : MonoBehaviour 
{
	public GameObject shot;
	public Transform shotSpawn;

	public float shotSpeed;

	/// <summary>
	/// Fires the weapon with force based on how long fire button was pressed.
	/// </summary>
	/// <param name="shotTime">Time that the fire button was pressed.</param>
	public void Fire(float shotTime)
	{
		if(!gameObject.activeSelf)
			return;

		GameObject projectile = Instantiate(shot, shotSpawn.position, shotSpawn.rotation) as GameObject;
		Mover script = projectile.GetComponent<Mover>();
		script.speed = shotSpeed * (Time.time - shotTime);
	}
}
