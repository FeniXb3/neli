﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum PlayerStates
{
	Idle,
	Active,
	Done,
	Dead
}


public class PlayerController : MonoBehaviour 
{

	private int activeWeaponIndex;
	private float nextFire;
	private bool isShooting;
	private float shotTime;
	private float playerDoneTime;
	private float distToGround;
	bool isgrounded;
	private int currentDirection;
	private PlayerStates state;
	
	private WeaponController weaponController;
	
	public float aimSpeed = 100;
	public GameObject weaponHandle;
	public List<GameObject> availableWeapons;
	private List<GameObject> weapons;
	public int ActiveWeaponIndex
	{
		get
		{
			return activeWeaponIndex;
		}
		set
		{
			if(CurrentWeapon != null)
				CurrentWeapon.SetActive(false);

			activeWeaponIndex = value % weapons.Count; 
		}
	}

	public GameObject CurrentWeapon
	{
		get 
		{ 
			return (ActiveWeaponIndex >= 0 && ActiveWeaponIndex < weapons.Count) ? weapons[ActiveWeaponIndex] : null; 
		}
	}

	public float fireRate;
	public int maxHealth = 100;
	public int health = 100;

	public float playerDoneDelay;

	public PlayerStates State
	{
		get { return state; }
		set
		{
			if(state == PlayerStates.Active)
			{
				if(CurrentWeapon != null)
					CurrentWeapon.SetActive(false);
			}

			switch(value)
			{
			case PlayerStates.Dead:
				transform.Rotate(transform.forward, 90);
				//gameObject.collider.enabled = false;
				break;
			case PlayerStates.Active:
				if(isgrounded)
					GetComponent<Rigidbody>().AddForce(Vector3.up * 150);
				break;
			default:
				break;
			}

			state = value;
		}
	}

	void Start()
	{
		activeWeaponIndex = -1;
		distToGround = GetComponent<Collider>().bounds.extents.y;
		currentDirection = 1;

		PrepareWeapons();
	}

	/// <summary>
	/// Prepares weapons.
	/// </summary>
	void PrepareWeapons()
	{
		weapons = new List<GameObject>();
		
		foreach(var weaponPrefab in availableWeapons)
		{
			GameObject weapon = GameObject.Instantiate(weaponPrefab, weaponHandle.transform.position, Quaternion.identity) as GameObject;
			weapon.transform.parent = weaponHandle.transform;
			weapon.SetActive(false);
			weapons.Add(weapon);
		}
	}

	void OnCollisionEnter(Collision theCollision)
	{
		if(theCollision.gameObject.tag == "Terrain")
		{
			if(theCollision.contacts.Length > 0 && theCollision.contacts[0].normal.y > 0)
			{
				isgrounded = true;
			}
		}
	}	

	void OnCollisionExit(Collision theCollision)
	{
		if(theCollision.gameObject.tag == "Terrain")
		{
			isgrounded = false;
		}
	}

	void Update ()
	{
		if(State != PlayerStates.Active)
			return;
		
		CheckWeaponChange();
		if(CurrentWeapon != null)
		{
			weaponController = CurrentWeapon.GetComponent<WeaponController>();
			AimWeapon();
			CheckFire();
		}
		
		Move();
		Jump();

		PreventMoveOnZ();
	}

	/// <summary>
	/// Ensures that the player will not change it's position on Z axis.
	/// </summary>
	void PreventMoveOnZ()
	{
		transform.position = new Vector3(transform.position.x, transform.position.y, 0);
	}

	/// <summary>
	/// Rotates and/or moves the player left or right.
	/// </summary>
	void Move()
	{
		int dir = 0;
		bool rotate = false;
		if(Input.GetKey(KeyCode.LeftArrow))
		{
			dir = -1;
		}
		else if(Input.GetKey(KeyCode.RightArrow))
		{
			dir = 1;
		}

		rotate = dir != currentDirection;

		if(rotate)
		{
			if(dir < 0)
				transform.Rotate(new Vector3(0, 180, 0));
			else if(dir > 0)
				transform.Rotate(new Vector3(0, 180, 0));

			if(dir != 0)
			{
				currentDirection = dir;
			}
		}
		else
		{
			//if(isgrounded)
			GetComponent<Rigidbody>().AddForce(dir * Vector3.right * Time.deltaTime * 1000);
			//transform.Translate(dir * Vector3.right * Time.deltaTime * 10);
		}

	}

	/// <summary>
	/// Jumps, if the player is grounded. 
	/// </summary>
	void Jump()
	{
		if(isgrounded && Input.GetButton("Jump"))
		{
			GetComponent<Rigidbody>().AddForce(10000 * Vector3.up * Time.deltaTime);
		}
	}

	/// <summary>
	/// Updates the angle witch the weapon is pointing at.
	/// </summary>
	void AimWeapon()
	{
		float angle = Input.GetAxis ("Vertical");
		Vector3 rotationPoint = transform.position;
		Vector3 axis = transform.forward;
		float angleZ = weaponHandle.transform.rotation.eulerAngles.z;
		float angleMargin = 45.0f;

		if((angleZ >= 0 && angleZ < 90 - angleMargin)
		   || (angleZ >= 90 - angleMargin && angleZ <= 180 && angle < 0)
		   || (angleZ > 270 + angleMargin && angleZ < 360)
		   || (angleZ <= 270 + angleMargin && angleZ >= 180 && angle > 0))
		{
			weaponHandle.transform.RotateAround (rotationPoint, axis, angle * aimSpeed * Time.deltaTime);
		}
	}


	void CheckFire()
	{
		if (Input.GetButton("Fire1") && Time.time > nextFire && !isShooting)
		{
			if(CurrentWeapon.activeSelf)
			{
				shotTime = Time.time;
				isShooting = true;
				nextFire = Time.time + fireRate;
			}
		}
		else if(Input.GetButtonUp("Fire1"))
		{
			if(CurrentWeapon.activeSelf)
			{
				weaponController.Fire(shotTime);
				isShooting = false;
			}
		}
	}

	/// <summary>
	/// Checks if the key for changing weapon has been pressed and changes the weapon if needed.
	/// </summary>
	void CheckWeaponChange()
	{
		if (Input.GetButtonUp("WeaponNext"))
		{
			ActiveWeaponIndex++;

			if(CurrentWeapon != null && !CurrentWeapon.activeSelf)
				CurrentWeapon.SetActive(true);
		}
	}

	/// <summary>
	/// Hurts the player for given amount of damage and kills it if health reaches 0.
	/// </summary>
	/// <param name="damage">Amount of damage dealt to player.</param>
	void TakeDamage(int damage)
	{
		if(State == PlayerStates.Dead)
			return;

		if(health - damage <= 0)
		{
			health = 0;
			State = PlayerStates.Dead;
		}
		else
		{
			health -= damage;
		}
	}

	/// <summary>
	/// Immidiately kills player.
	/// </summary>
	void Kill()
	{
		TakeDamage(maxHealth);
	}
}
